package miu.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.LocalDateTime;

//@SpringBootApplication
public class ClientApplication {

	public static void main(String[] args) throws InterruptedException {
//		SpringApplication.run(ClientApplication.class, args);

		Flux<Stock> result = WebClient.create("http://localhost:8080/stock")
				.get()
				.retrieve()
				.bodyToFlux(Stock.class);
		result.subscribe(s->{
			System.out.print(LocalDateTime.now()+" : ");
			System.out.println(s);
		});
		Thread.sleep(77000);
	}


}
