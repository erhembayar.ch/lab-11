package miu.client;

public class Stock {

    String value;
    public Stock(String value){
        super();
        this.value = value;
    }

    public  Stock(){
        super();
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String toString() {
        return "stock has : " + value;
    }

}
