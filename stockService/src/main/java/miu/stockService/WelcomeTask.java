package miu.stockService;

import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.beans.Customizer;
import java.text.DateFormat;
import java.time.Duration;
import java.util.Calendar;
import java.util.Date;

@RestController
public class WelcomeTask {

    @GetMapping(value="/stock", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Stock> welcome(){

        Flux<Stock> customerFlux = Flux.just(
                new Stock("1"),
                new Stock("2"),
        new Stock("3"),
        new Stock("4"),
        new Stock("5")
        ).delayElements(Duration.ofSeconds(3));

        return customerFlux;
    }

}
